﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LaborIT_Teste.Models;

namespace LaborIT_Teste.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ModelsController : ControllerBase
    {
        private readonly LaborIT_TesteContext _context;

        public ModelsController(LaborIT_TesteContext context)
        {
            _context = context;
        }

        // GET: api/Models
        [HttpGet]
        public IEnumerable<Models.Models> GetModels()
        {
            return _context.Models;
        }

        // GET: api/Models/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetModels([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var models = await _context.Models.FindAsync(id);

            if (models == null)
            {
                return NotFound();
            }

            return Ok(models);
        }

        // PUT: api/Models/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutModels([FromRoute] long id, [FromBody] Models.Models models)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != models.Id)
            {
                return BadRequest();
            }

            _context.Entry(models).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ModelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Models
        [HttpPost]
        public async Task<IActionResult> PostModels([FromBody] Models.Models models)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Models.Add(models);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetModels", new { id = models.Id }, models);
        }

        // DELETE: api/Models/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteModels([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var models = await _context.Models.FindAsync(id);
            if (models == null)
            {
                return NotFound();
            }

            _context.Models.Remove(models);
            await _context.SaveChangesAsync();

            return Ok(models);
        }

        private bool ModelsExists(long id)
        {
            return _context.Models.Any(e => e.Id == id);
        }
    }
}