﻿using System.ComponentModel.DataAnnotations;

namespace LaborIT_Teste.Models
{
    public class Vehicles
    {
        public long Id { get; set; }
        [Required]
        public string Value { get; set; }
        public long BrandId { get; set; }
        public long ModelId { get; set; }
        public int YearModel { get; set; }
        public string Fuel { get; set; }
    }
}
