﻿using System.ComponentModel.DataAnnotations;

namespace LaborIT_Teste.Models
{
    public class Models
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
        public long BrandId { get; set; }
    }
}
