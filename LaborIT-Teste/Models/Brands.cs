﻿using System.ComponentModel.DataAnnotations;

namespace LaborIT_Teste.Models
{
    public class Brands
    {
        public long Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
