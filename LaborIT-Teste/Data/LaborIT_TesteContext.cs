﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LaborIT_Teste.Models;

namespace LaborIT_Teste.Models
{
    public class LaborIT_TesteContext : DbContext
    {
        public LaborIT_TesteContext (DbContextOptions<LaborIT_TesteContext> options)
            : base(options)
        {
        }

        public DbSet<LaborIT_Teste.Models.Brands> Brands { get; set; }

        public DbSet<LaborIT_Teste.Models.Models> Models { get; set; }

        public DbSet<LaborIT_Teste.Models.Vehicles> Vehicles { get; set; }
    }
}
